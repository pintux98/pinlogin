package it.pintux98.bot.cmds.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.config.data.GuildData;
import it.pintux98.bot.config.data.LangData;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;

import java.awt.*;
import java.time.Instant;

public class Settings extends Command {

    public Settings() {
        this.name = "set";
        this.help = "set pinloginchannel/pinloginprefix/botprefix/pinloginrole/pinloginalert <value>";
        this.guildOnly = true;
        this.aliases = new String[]{"settings", "setting"};
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
    }

    @Override
    protected void execute(CommandEvent event) {
        if (event.getArgs().isEmpty() || event.getArgs().split("\\s").length < 2) {
            event.reply(new EmbedBuilder()
                    .setColor(event.isFromType(ChannelType.TEXT) ? event.getSelfMember().getColor() : Color.GREEN)
                    .setTitle("Error")
                    .addField("Usage:", "`" + event.getClient().getPrefix() + getHelp() + "`", false)
                    .setFooter("Author: pintux#2007")
                    .setTimestamp(Instant.now())
                    .build());
        } else {
            FileManager fileManager = Core.getFileManager();
            GuildData guildData = fileManager.getGuildData();
            LangData langData = fileManager.getLangData();
            String[] args = event.getArgs().split("\\s");
            long tempValue;
            TextChannel tempchannel;
            switch (args[0].toLowerCase()) {
                case "pinloginchannel":
                    try {
                        tempValue = Long.parseLong(args[1]);
                        tempchannel = event.getGuild().getTextChannelById(tempValue);
                        if (tempchannel != null) {
                            guildData.setPinloginchannel(tempValue);
                            event.reactSuccess();
                            MessageHistory.getHistoryFromBeginning(tempchannel).queue(messageHistory -> {
                                tempchannel.purgeMessages(messageHistory.getRetrievedHistory());
                                tempchannel.sendMessageEmbeds(new EmbedBuilder()
                                        .setColor(Color.RED)
                                        .setTitle(guildData.getBotsecurityprefix())
                                        .setDescription(langData.getPinlogin())
                                        .setFooter("Author: pintux#2007")
                                        .setTimestamp(Instant.now())
                                        .build()
                                ).queue();
                            });
                        } else {
                            event.replyWarning("No channel found with that ID");
                        }
                    } catch (NumberFormatException e) {
                        event.replyError("Invalid argument. Should be a number");
                    }
                    break;
                case "pinloginprefix":
                    guildData.setBotsecurityprefix(args[1]);
                    event.reactSuccess();
                    break;
                case "botprefix":
                    guildData.setBotnormalprefix(args[1]);
                    event.reactSuccess();
                    event.getGuild().getSelfMember().modifyNickname(args[1]).queue();
                    break;
                case "pinloginrole":
                    Role tempRole;
                    if (!event.getMessage().getMentions().getRoles().isEmpty()) {
                        tempRole = event.getMessage().getMentions().getRoles().get(0);
                    } else {
                        try {
                            tempValue = Long.parseLong(args[1]);
                            tempRole = event.getGuild().getRoleById(tempValue);
                        } catch (NumberFormatException e) {
                            event.replyError("Invalid argument. Should be a number");
                            return;
                        }
                    }
                    if (tempRole != null) {
                        guildData.setPinloginrole(tempRole.getIdLong());
                        event.reactSuccess();
                    } else {
                        event.replyWarning("No role found with that ID");
                    }
                    break;
                case "pinloginalert":
                    try {
                        tempValue = Long.parseLong(args[1]);
                        tempchannel = event.getGuild().getTextChannelById(tempValue);
                        if (tempchannel != null) {
                            guildData.setPinloginalert(tempValue);
                            event.reactSuccess();
                        } else {
                            event.replyWarning("No channel found with that ID");
                        }
                    } catch (NumberFormatException e) {
                        event.replyError("Invalid argument. Should be a number");
                    }
                    break;
                default:
                    event.replyError("Command not found");
                    break;
            }
            fileManager.saveGuildData(guildData);
            fileManager.reload();
        }
    }

}
