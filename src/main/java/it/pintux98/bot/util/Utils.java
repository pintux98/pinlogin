package it.pintux98.bot.util;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.CommandOption;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.config.data.GuildData;
import it.pintux98.bot.config.data.LangData;
import it.pintux98.bot.config.data.UserData;
import it.pintux98.common.security.LoginData;
import it.pintux98.common.socket.connection.packet.Packet;
import it.pintux98.server.db.entity.LinkedUser;
import it.pintux98.server.db.repository.LinkRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static final String CHECK_EMOJI = "\u2705";
    public static final String DENY_EMOJI = "\u26d4";

    @Autowired
    private LinkRepository linkRepository;

    private static Utils instance;

    public static Utils getInstance() {
        return instance == null ? new Utils() : instance;
    }

    public Utils() {
        instance = this;
    }

    public EmbedBuilder getDmError() {
        FileManager fileManager = Core.getFileManager();
        LangData langData = fileManager.getLangData();
        return new EmbedBuilder()
                .setColor(Color.RED)
                .setTitle("Error")
                .setDescription(langData.getCantsenddm())
                .setFooter("Author: pintux#2007")
                .setTimestamp(Instant.now());
    }

    public void handlePinLoginPacket(Packet packet) {
        Object[] arr = packet.toArray();
        FileManager fileManager = Core.getFileManager();
        GuildData guildData = fileManager.getGuildData();
        MessageChannel alertChannel = Core.getJda().getTextChannelById(guildData.getPinloginalert());
        LangData langData = fileManager.getLangData();
        if (Core.getJda().getUserById((String) arr[1]) != null) {
            final int code = (int) arr[0];
            final User user = Core.getJda().getUserById((String) arr[1]);
            final String mcName = (String) arr[2];
            if (user != null) {
                switch (code) {
                    case 190:
                    case 191: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getLinkcode().replaceAll("%player%", mcName).replaceAll("%code%", mcName.hashCode() + ""))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> {
                        }, throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        UserData userData = fileManager.getUserData(user.getIdLong());
                        userData.setUsingPinLogin(true);
                        fileManager.saveUserData(user.getIdLong(), userData);
                        break;
                    }
                    case 192: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getLinklinked().replaceAll("%player%", mcName))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> {
                        }, throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 193: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getUnlinksuccess().replaceAll("%player%", mcName))
                                .setColor(Color.RED)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 194: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getUnlinkfaildiscord())
                                .setColor(Color.RED)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 195: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getUnlinkfailname().replaceAll("%player%", mcName))
                                .setColor(Color.RED)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 196: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getUnlinkfailunknown().replaceAll("%player%", mcName))
                                .setColor(Color.RED)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 197: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getLoginaccept().replaceAll("%player%", mcName))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 198: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getLogindeny().replaceAll("%player%", mcName))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 199: {
                        final String ip = (String) arr[3];
                        if (Core.getLoginData().get(user.getIdLong()) != null) {
                            Core.getLoginData().remove(user.getIdLong());
                        }
                        Core.getLoginData().put(user.getIdLong(), new LoginData(mcName, ip));
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getLoginalert().replaceAll("%player%", "`" + mcName + "`").replaceAll("%ip%", "`" + ip + "`"))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> {
                            message.addReaction(Emoji.fromUnicode(Utils.CHECK_EMOJI)).queue();
                            message.addReaction(Emoji.fromUnicode(Utils.DENY_EMOJI)).queue();
                        }, throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 1910: {
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getInvaliduser().replaceAll("%player%", mcName))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message ->
                                message.delete().queueAfter(5, TimeUnit.SECONDS), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                    case 1911: {
                        final String cip = (String) arr[3];
                        final String oip = (String) arr[4];
                        final String disID = (String) arr[5];
                        final String modified = (String) arr[6];
                        String disTag = "No member found on the server!";
                        try {
                            disTag = Core.getJda().getUserById(disID).getAsTag();
                        } catch (Exception ignored) {
                        }
                        final String finalDisTag = disTag;
                        user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                .setTitle(guildData.getBotsecurityprefix())
                                .setDescription(langData.getUserinfo()
                                        .replaceAll("%player%", mcName)
                                        .replaceAll("%curip%", cip)
                                        .replaceAll("%oldip%", oip)
                                        .replaceAll("%disID%", disID)
                                        .replaceAll("%distag%", finalDisTag)
                                        .replaceAll("%modified%", modified))
                                .setColor(Color.GREEN)
                                .setFooter("Author: pintux#2007")
                                .setTimestamp(Instant.now())
                                .build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        }), throwable -> {
                            if (alertChannel != null) {
                                alertChannel.sendMessage(user.getAsMention()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                                alertChannel.sendMessageEmbeds(getDmError().build()).queue(message -> message.delete().queueAfter(1, TimeUnit.MINUTES));
                            }
                        });
                        break;
                    }
                }
            }
        }
    }

    public List<LinkedUser> getLinkedUsers(SlashCommandEvent event) {
        List<LinkedUser> linkedUsers = new ArrayList<>();

        if (event.hasOption(CommandOption.MC_NAME.getOption())) {
            OptionMapping MCoption = event.getOption(CommandOption.MC_NAME.getOption());
            if (MCoption != null) {
                String mcName = MCoption.getAsString();
                linkedUsers = linkRepository.findLinkedUserByName(mcName);
            }
        }
        if (event.hasOption(CommandOption.USER.getOption())) {
            OptionMapping userOption = event.getOption(CommandOption.USER.getOption());
            if (userOption != null) {
                User user = userOption.getAsUser();
                linkedUsers = linkRepository.findLinkedUsersByDiscordID(user.getId());
            }
        }
        if (event.hasOption(CommandOption.CODE.getOption())) {
            OptionMapping codeOption = event.getOption(CommandOption.CODE.getOption());
            if (codeOption != null) {
                String code = codeOption.getAsString();
                linkedUsers.add(linkRepository.findLinkedUserByBackupCode(code));
            }
        }
        return linkedUsers;
    }

    public void loadChannels() {
        FileManager fileManager = Core.getFileManager();
        GuildData guildData = fileManager.getGuildData();
        LangData langData = fileManager.getLangData();
        MessageChannel loginChannel = Core.getJda().getTextChannelById(guildData.getPinloginchannel());
        if (loginChannel != null) {
            MessageHistory.getHistoryFromBeginning(loginChannel).queue(messageHistory -> {
                loginChannel.purgeMessages(messageHistory.getRetrievedHistory());
                loginChannel.sendMessageEmbeds(new EmbedBuilder()
                        .setColor(Color.RED)
                        .setTitle(guildData.getBotsecurityprefix())
                        .setDescription(langData.getPinlogin())
                        .setFooter("Author: pintux#2007")
                        .setTimestamp(Instant.now())
                        .build()
                ).queue();
                //loginChannel.sendFile(new FileIO("guild", "pinlogin.mp4").getFile(false), AttachmentOption.SPOILER).queue(message -> {
            }, throwable -> {
                Core.getLogger().info("No pinlogin.mp4 file found.");
            });
        }
    }
}
