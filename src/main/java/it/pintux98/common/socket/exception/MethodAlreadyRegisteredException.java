package it.pintux98.common.socket.exception;

public class MethodAlreadyRegisteredException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance.
     */
    public MethodAlreadyRegisteredException() {
    }

}