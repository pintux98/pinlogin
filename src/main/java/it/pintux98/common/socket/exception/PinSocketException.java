package it.pintux98.common.socket.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PinSocketException implements Thread.UncaughtExceptionHandler {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        logger.error("An exception has been captured");
        logger.error("Thread: " + thread.getId());
        logger.error("Exception: " + throwable.getClass().getName() + ": " + throwable.getMessage());
        logger.error("Thread status: " + thread.getState());
        logger.error("Stack Trace: ");
        throwable.printStackTrace();
    }
}
