package it.pintux98.server.db.repository;

import it.pintux98.server.db.entity.StaffUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StaffRepository extends CrudRepository<StaffUser, Long> {

    @Query(value = "select staff from StaffUser staff where staff.discordID=:discordID")
    Optional<StaffUser> searchByDiscordID(String discordID);

}
