package it.pintux98.bot.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import it.pintux98.Core;
import it.pintux98.bot.config.data.GuildData;
import it.pintux98.bot.config.data.LangData;
import it.pintux98.bot.config.data.UserData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileManager {

    private final ObjectMapper objectMapper;
    private LangData langData;
    private GuildData guildData;
    private FileIO fileIO;

    public FileManager() {
        objectMapper = new ObjectMapper(new YAMLFactory());
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        guildData = getGuild();
        langData = getLang();
    }

    public UserData getUserData(long userID) {
        fileIO = new FileIO("users", userID + ".yml");
        File file = fileIO.getFile(false);
        UserData userData;
        try {
            userData = objectMapper.readValue(file, UserData.class);
            Core.getLogger().info("Loading UserData with ID " + userID);
        } catch (IOException ignored) {
            userData = new UserData();
            userData.setPinloginlogs(new ArrayList<>());
            userData.setUsingPinLogin(false);
            Core.getLogger().info("Creating new UserData with ID " + userID);
        }
        return userData;
    }

    public void saveUserData(long userID, UserData userData) {
        deleteDataFile("users", userID + ".yml");
        try {
            objectMapper.writeValue(new File("PinLoginPremium/users/" + userID + ".yml"), userData);
            Core.getLogger().info("Creating new UserData with ID " + userID);
        } catch (Exception ignored) {
        }
    }

    public void deleteDataFile(String path, String fileName) {
        fileIO = new FileIO(path, fileName + ".yml");
        File file = fileIO.getFile(false);
        if (file.exists()) {
            file.delete();
            Core.getLogger().info("Deleting file " + path + "/" + fileName);
        }
    }

    private GuildData getGuild() {
        fileIO = new FileIO("guild", "guild.yml");
        File file = fileIO.getFile(true);
        GuildData guildData;
        try {
            guildData = objectMapper.readValue(file, GuildData.class);
            Core.getLogger().info("Loading GuildData");
        } catch (IOException ignored) {
            guildData = new GuildData();
            guildData.setBotnormalprefix("PinLoginPremium");
            guildData.setBotsecurityprefix("PinLoginPremium");
            guildData.setPinloginchannel(0);
            guildData.setServerhost("beta.pintux98.it");
            guildData.setServerport(28185);
            guildData.setBottoken("INSERT_TOKEN");
            guildData.setStatus("Securing accounts");
            Core.getLogger().info("Creating new GuildData");
        }
        return guildData;
    }

    public void saveGuildData(GuildData guildData) {
        fileIO = new FileIO("guild", "guild.yml");
        File file = fileIO.getFile(true);
        if (file.delete()) {
            try {
                objectMapper.writeValue(new File("PinLoginPremium/guild/" + "guild.yml"), guildData);
                Core.getLogger().info("Saving GuildData");
            } catch (Exception ignored) {
            }
        }
    }

    private LangData getLang() {
        fileIO = new FileIO("guild", "lang.yml");
        File file = fileIO.getFile(true);
        LangData langData;
        try {
            langData = objectMapper.readValue(file, LangData.class);
            Core.getLogger().info("Loading LangData");
            return langData;
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
        return new LangData();
    }

    public String[] getFiles(String folder) {
        fileIO = new FileIO(folder, "");
        return fileIO.getFiles();
    }

    public GuildData getGuildData() {
        return guildData;
    }

    public LangData getLangData() {
        return langData;
    }

    public void reload() {
        guildData = getGuild();
        langData = getLang();
    }
}
