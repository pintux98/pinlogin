package it.pintux98.bot.config.data;

import java.util.List;

public class UserData {

    private List<String> pinloginlogs;
    private boolean usingPinLogin;

    public UserData() {
    }

    public List<String> getPinloginlogs() {
        return pinloginlogs;
    }

    public void setPinloginlogs(List<String> pinloginlogs) {
        this.pinloginlogs = pinloginlogs;
    }

    public boolean isUsingPinLogin() {
        return usingPinLogin;
    }

    public void setUsingPinLogin(boolean usingPinLogin) {
        this.usingPinLogin = usingPinLogin;
    }

}
