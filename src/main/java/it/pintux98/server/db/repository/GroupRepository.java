package it.pintux98.server.db.repository;

import it.pintux98.server.db.entity.PinGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface GroupRepository extends CrudRepository<PinGroup, Long> {

}
