package it.pintux98.bot.config.data;

public class GuildData {

    private long pinloginchannel;
    private int serverport;
    private String serverhost, botsecurityprefix, botnormalprefix;
    private String bottoken;
    private long pinloginrole;
    private long pinloginalert;
    private String cmduserprefix;
    private String cmdadminprefix;
    private String status;

    public GuildData() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCmduserprefix() {
        return cmduserprefix;
    }

    public void setCmduserprefix(String cmduserprefix) {
        this.cmduserprefix = cmduserprefix;
    }

    public String getCmdadminprefix() {
        return cmdadminprefix;
    }

    public void setCmdadminprefix(String cmdadminprefix) {
        this.cmdadminprefix = cmdadminprefix;
    }

    public long getPinloginalert() {
        return pinloginalert;
    }

    public void setPinloginalert(long pinloginalert) {
        this.pinloginalert = pinloginalert;
    }

    public long getPinloginrole() {
        return pinloginrole;
    }

    public void setPinloginrole(long pinloginrole) {
        this.pinloginrole = pinloginrole;
    }

    public String getBottoken() {
        return bottoken;
    }

    public void setBottoken(String bottoken) {
        this.bottoken = bottoken;
    }

    public long getPinloginchannel() {
        return pinloginchannel;
    }

    public void setPinloginchannel(long pinloginchannel) {
        this.pinloginchannel = pinloginchannel;
    }

    public int getServerport() {
        return serverport;
    }

    public void setServerport(int serverport) {
        this.serverport = serverport;
    }

    public String getServerhost() {
        return serverhost;
    }

    public void setServerhost(String serverhost) {
        this.serverhost = serverhost;
    }

    public String getBotsecurityprefix() {
        return botsecurityprefix;
    }

    public void setBotsecurityprefix(String botsecurityprefix) {
        this.botsecurityprefix = botsecurityprefix;
    }

    public String getBotnormalprefix() {
        return botnormalprefix;
    }

    public void setBotnormalprefix(String botnormalprefix) {
        this.botnormalprefix = botnormalprefix;
    }

    @Override
    public String toString() {
        return "GuildData{" +
                "pinloginchannel=" + pinloginchannel +
                ", serverport=" + serverport +
                ", serverhost='" + serverhost + '\'' +
                ", botsecurityprefix='" + botsecurityprefix + '\'' +
                ", botnormalprefix='" + botnormalprefix + '\'' +
                ", bottoken='" + bottoken + '\'' +
                ", pinloginrole=" + pinloginrole +
                ", pinloginalert=" + pinloginalert +
                ", cmduserprefix='" + cmduserprefix + '\'' +
                ", cmdadminprefix='" + cmdadminprefix + '\'' +
                '}';
    }
}
