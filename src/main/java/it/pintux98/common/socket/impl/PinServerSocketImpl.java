package it.pintux98.common.socket.impl;

import it.pintux98.Core;
import it.pintux98.common.security.WhitelistIP;
import it.pintux98.common.socket.IDGenerator;
import it.pintux98.common.socket.PinClient;
import it.pintux98.common.socket.PinServer;
import it.pintux98.common.socket.connection.Callback;
import it.pintux98.common.socket.connection.packet.DefaultCallbackPacket;
import it.pintux98.common.socket.connection.packet.Packet;
import it.pintux98.common.socket.exception.MethodAlreadyRegisteredException;
import it.pintux98.common.socket.exception.PinSocketException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class PinServerSocketImpl implements PinServer {
    private Logger logger = LogManager.getLogger("Server");
    private Map<String, Callback<Packet, Socket>> registry;
    private Set<PinClient> clients;
    private boolean running;
    private Thread main;

    private final int port;
    private final IDGenerator idGenerator;
    private ServerSocket serverSocket;

    public PinServerSocketImpl(int port){
        this.port = port;
        this.idGenerator = IDGenerator.getGenerator();
        clients = new HashSet<>();
        registry = new ConcurrentHashMap<>();
        runServer();
    }

    @Override
    public void runServer() {
        main = new Thread(() -> {
            main.setUncaughtExceptionHandler(new PinSocketException());
            try {
                serverSocket = new ServerSocket(port);
                serverSocket.setReuseAddress(true);
                running = true;
                debug("Server started");
            } catch (IOException e) {
                debug("Startup failed...");
                e.printStackTrace();
            }
            while (running && !main.isInterrupted()) {
                try {
                    Socket socket = serverSocket.accept();
                    WhitelistIP whitelist = new WhitelistIP();
                    if (whitelist.load()) {
                        if (!whitelist.allowed(socket.getRemoteSocketAddress().toString())) {
                            Core.sendLogMessage("Could not verify IP " + socket.getRemoteSocketAddress() + " - client disconnected forcefully", false);
                            socket.close();
                        } else {
                            PinClient con = new PinClientSocketImpl(this, socket);
                            con.startClient();
                            clients.add(con);
                        }
                    } else {
                        PinClient con = new PinClientSocketImpl(this, socket);
                        con.startClient();
                        clients.add(con);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        main.start();
        this.registerPacket("net.client.login", ((packet, socket) -> {
            Socket socket1 = (Socket) socket;
            debug("New Client login [" + packet.signature + "@" + socket1.getRemoteSocketAddress() + "]");
            PinClient con = this.castFromSocket(socket1);
            if (con == null) {
                con = new PinClientSocketImpl(packet.signature,this, socket1);
                clients.add(con);
                con.startClient();
            } else {
                if (con.getClientID().equalsIgnoreCase("NULL")) {
                    con.setClientID(packet.signature);
                }
            }
        }));
        this.registerPacket("net.client.respond.pong", (packet, socket) -> {
            PinClient con = this.castFromSocket((Socket) socket);
            sendPacket(con, new DefaultCallbackPacket());
            debug("Client " + con + " responded with pong.");
        });
        this.registerPacket("net.client.connection.disconnect.clean", (packet, socket) -> {
            PinClient con = this.castFromSocket((Socket) socket);
            debug("Client " + con + " disconnected cleanly");
            if (con != null && con.getSocket() != null)
                removeClient(con);
        });
    }

    @Override
    public void stopServer() {
        running = false;
        serverSocket = null;
        for (PinClient con : getClients()) {
            con.stopClient();
        }
        clients.clear();
        registry.clear();
        main.interrupt();
    }

    @Override
    public void addClient(PinClient client) {
        client.startClient();
        clients.add(client);
    }

    @Override
    public PinClient castFromSocket(Socket socket) {
        for (PinClient con : clients) {
            String localSocket = con.getSocket().getInetAddress().getHostAddress() + ":" + con.getSocket().getPort();
            String remoteSocket = socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
            if (localSocket.equals(remoteSocket)) {
                return con;
            }
        }
        return null;
    }

    @Override
    public void removeClient(PinClient con) {
        Predicate<PinClient> connectionPredicate = clientConnection -> clientConnection.getClientID().equalsIgnoreCase(con.getClientID());
        if (getClients().removeIf(connectionPredicate)) {
            con.stopClient();
        }
    }

    @Override
    public void registerPacket(String key, Callback<Packet, Socket> task) {
        debug("Registering key " + key);
        if (registry.containsKey(key))
            throw new MethodAlreadyRegisteredException();
        registry.put(key, task);
    }
    
    @Override
    public void sendPacket(PinClient receiver, Packet packet) {
        debug("Sending packet (" + packet.getId() + ") to " + receiver);
        if (!receiver.getSocket().isConnected()) {
            removeClient(receiver);
            debug("Client " + receiver + " disconnected");
            return;
        }
        receiver.sendPacket(packet);
    }

    @Override
    public void broadcastPacket(Packet packet) {
        debug("Sending packet to all clients");
        for (PinClient con : clients) {
            this.sendPacket(con, packet);
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public Map<String, Callback<Packet, Socket>> getRegistry() {
        if (registry == null) {
            registry = new ConcurrentHashMap<>();
        }
        return registry;
    }

    @Override
    public Set<PinClient> getClients() {
        if (clients == null) {
            clients = new HashSet<>();
        }
        return clients;
    }

    @Override
    public void debug(String debug) {
        logger.info(debug);
        Core.sendLogMessage(debug, false);
    }
}
