package it.pintux98.common.file;

import it.pintux98.Core;
import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings({"unchecked", "rawtypes"})
public class YamlConfig implements ConfigurationAdapter {
    private static final Logger logger = LogManager.getLogger(YamlConfig.class.getName());

    private final Yaml yaml;
    private final File file;
    private Map<String, Object> config;
    private String resourcePath;
    private ClassLoader classLoader = null;

    public YamlConfig(String fileName) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        yaml = new Yaml(options);
        classLoader = Core.class.getClassLoader();
        this.file = new File("PinServer/" + fileName);
        this.resourcePath = fileName;
    }

    private static void debug(String message) {
        logger.info(message);
    }

    @Override
    public void load() {
        try {
            saveResource();
            file.createNewFile();
            try (InputStream is = new FileInputStream(file)) {
                try {
                    config = (Map) yaml.load(is);
                } catch (Exception ex) {
                    throw new RuntimeException(
                            "Invalid configuration encountered - this is a configuration error and NOT a bug!", ex);
                }
            }

            if (config == null) {
                config = new CaseInsensitiveMap<>();
            } else {
                config = new CaseInsensitiveMap<>(config);
            }
        } catch (IOException ex) {
            throw new RuntimeException("Could not load configuration!", ex);
        }
    }

    private <T> T get(String path, T def) {
        return get(path, def, config);
    }

    private <T> T get(String path, T def, Map submap) {
        int index = path.indexOf('.');
        if (index == -1) {
            Object val = submap.get(path);
            if (val == null && def != null) {
                val = def;
                submap.put(path, def);
                save();
            }
            return (T) val;
        } else {
            String first = path.substring(0, index);
            String second = path.substring(index + 1, path.length());
            Map sub = (Map) submap.get(first);
            if (sub == null) {
                sub = new LinkedHashMap();
                submap.put(first, sub);
            }
            return get(second, def, sub);
        }
    }

    public void set(String path, Object val) {
        set(path, val, config);
    }

    private void set(String path, Object val, Map submap) {
        int index = path.indexOf('.');
        if (index == -1) {
            if (val == null) {
                submap.remove(path);
            } else {
                submap.put(path, val);
            }
            save();
        } else {
            String first = path.substring(0, index);
            String second = path.substring(index + 1, path.length());
            Map sub = (Map) submap.get(first);
            if (sub == null) {
                sub = new LinkedHashMap();
                submap.put(first, sub);
            }
            set(second, val, sub);
        }
    }

    private void save() {
        try {
            try (FileWriter wr = new FileWriter(file)) {
                yaml.dump(config, wr);
            }
        } catch (IOException ex) {

        }
    }

    @Override
    public int getInt(String path, int def) {
        return get(path, def);
    }

    @Override
    public long getLong(String path, long def) {
        return get(path, def);
    }

    @Override
    public String getString(String path, String def) {
        return get(path, def);
    }

    @Override
    public boolean getBoolean(String path, boolean def) {
        return get(path, def);
    }

    @Override
    public Collection<?> getList(String path, Collection<?> def) {
        return get(path, def);
    }

    public void saveResource() {
        if (resourcePath == null || resourcePath.equals("")) {
            throw new IllegalArgumentException("ResourcePath cannot be null or empty");
        }

        resourcePath = resourcePath.replace('\\', '/');
        InputStream in = getResource(resourcePath);
        if (in == null) {
            throw new IllegalArgumentException(
                    "The embedded resource '" + resourcePath + "' cannot be found in " + file);
        }

        File outFile = new File("PinServer/", resourcePath);
        int lastIndex = resourcePath.lastIndexOf('/');
        File outDir = new File("PinServer/", resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

        if (!outDir.exists()) {
            outDir.mkdirs();
        }

        try {
            if (!outFile.exists()) {
                OutputStream out = new FileOutputStream(outFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.close();
                in.close();
            } else {
                debug("Could not save " + outFile.getName() + " to " + outFile + " because "
                        + outFile.getName() + " already exists.");
            }
        } catch (IOException ex) {
            debug("Could not save " + outFile.getName() + " to " + outFile);
            ex.printStackTrace();
        }
    }

    public InputStream getResource(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("Filename cannot be null");
        }

        try {
            URL url = classLoader.getResource(filename);

            if (url == null) {
                return null;
            }

            URLConnection connection = url.openConnection();
            connection.setUseCaches(false);
            return connection.getInputStream();
        } catch (IOException ex) {
            return null;
        }
    }
}
