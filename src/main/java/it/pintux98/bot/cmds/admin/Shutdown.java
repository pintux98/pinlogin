package it.pintux98.bot.cmds.admin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import com.jagrosh.jdautilities.doc.standard.CommandInfo;
import com.jagrosh.jdautilities.examples.doc.Author;
import it.pintux98.Core;
import it.pintux98.bot.cmds.PCommand;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;

public class Shutdown extends PCommand {

    public Shutdown() {
        this.name = "shutdown";
        this.help = "shutdown";
        this.guildOnly = false;
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        event.reply("").queue();
        shutdown(event.getJDA());
    }

    private void shutdown(final JDA jda) {
        Core.getInstance().getServer().stopServer();
        jda.shutdown();
        System.exit(0);
    }

    @Override
    public String getPermission() {
        return "stop";
    }
}
