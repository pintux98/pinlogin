package it.pintux98.common.socket;

import it.pintux98.common.socket.connection.packet.Packet;

import java.net.Socket;

public interface PinClient {

    String getClientID();

    void setClientID(String clientID);
    String getServerID();
    Socket getSocket();

    void startClient();
    void stopClient();

    void sendPacket(Packet packet);

}
