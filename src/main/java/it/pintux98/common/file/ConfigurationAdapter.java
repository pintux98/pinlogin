package it.pintux98.common.file;

import java.util.Collection;

public interface ConfigurationAdapter {

    void load();

    int getInt(String path, int def);

    long getLong(String path, long def);

    String getString(String path, String def);

    boolean getBoolean(String path, boolean def);

    Collection<?> getList(String path, Collection<?> def);
}