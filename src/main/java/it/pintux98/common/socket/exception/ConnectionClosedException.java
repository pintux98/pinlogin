package it.pintux98.common.socket.exception;

public class ConnectionClosedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -1L;

    /**
     * Creates a new instance.
     */
    public ConnectionClosedException() {
    }

}
