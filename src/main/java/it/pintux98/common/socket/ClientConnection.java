package it.pintux98.common.socket;

import it.pintux98.Core;
import it.pintux98.common.socket.connection.packet.Packet;
import it.pintux98.common.socket.exception.PinSocketException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientConnection extends Thread {

    private final Logger logger = LogManager.getLogger("ClientThread");
    private final Socket socket;
    private final String serverID;
    private final ServerMain main;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String clientID;

    public ClientConnection(String clientID, Socket socket, ServerMain main) {
        this.socket = socket;
        this.main = main;
        this.serverID = main.getIdGenerator().generateToken(socket);
        this.clientID = clientID;
    }

    public ClientConnection(Socket socket, ServerMain main) {
        this("NULL", socket, main);
    }

    public Socket getSocket() {
        return socket;
    }

    public String getServerID() {
        return serverID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public void startClient() {
        try {
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());
            this.start();
        } catch (IOException e) {
            debug("I/O error");
            e.printStackTrace();
            main.removeClient(this);
        }
    }

    @Override
    public void run() {
        while (true) {
            this.setUncaughtExceptionHandler(new PinSocketException());
            try {
                Object obj = input.readObject();
                if (obj instanceof Packet) {
                    Packet packet = (Packet) obj;
                    debug("Packet received (" + packet.getId() + ") " + packet);
                    for (String reg : main.getRegistry().keySet()) {
                        if (packet.getId().equals(reg)) {
                            new Thread(() -> {
                                try {
                                    main.getRegistry().get(reg).respond(packet, socket);
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }).start();
                            break;
                        }
                    }
                } else {
                    debug("Received unknown object " + obj);
                }
            } catch (IOException | ClassNotFoundException e) {
                debug("I/O Exception in reading packet");
                Core.sendLogMessage(e.getMessage(), true);
                //e.printStackTrace();
                main.removeClient(this);
                break;
            }
        }
    }

    public synchronized void sendMessage(Packet packet) {
        debug("Sending packet (" + packet.getId() + ") to " + socket);
        if (!socket.isConnected()) {
            main.removeClient(this);
            debug("Client " + socket + " disconnected");
            return;
        }
        try {
            output.writeObject(packet);
            output.flush();
        } catch (IOException e) {
            main.removeClient(this);
            debug("Client " + socket + " disconnected");
        }
    }

    public void stopClient() {
        debug("Closing client " + serverID + "-" + clientID);
        try {
            if (socket != null) {
                socket.close();
            }
            if (input != null) {
                input.close();
            }

            if (output != null) {
                output.close();
            }
            this.interrupt();
        } catch (IOException e) {
            debug("Close exception");
            e.printStackTrace();
        }
    }

    public void debug(String debug) {
        Core.sendLogMessage(debug, false);
        logger.info(debug);
    }
}
