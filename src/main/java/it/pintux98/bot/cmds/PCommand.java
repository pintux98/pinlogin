package it.pintux98.bot.cmds;

import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.handler.PermissionHandler;

public abstract class PCommand extends SlashCommand {

    public abstract String getPermission();

    public boolean isServerDependent(){return false;}

}
