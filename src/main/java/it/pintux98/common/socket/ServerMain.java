package it.pintux98.common.socket;

import it.pintux98.Core;
import it.pintux98.common.security.WhitelistIP;
import it.pintux98.common.socket.connection.Callback;
import it.pintux98.common.socket.connection.packet.DefaultCallbackPacket;
import it.pintux98.common.socket.connection.packet.Packet;
import it.pintux98.common.socket.exception.MethodAlreadyRegisteredException;
import it.pintux98.common.socket.exception.PinSocketException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class ServerMain {

    private final Logger logger = LogManager.getLogger("Server");

    private final int port;
    private final IDGenerator idGenerator;
    private final Set<ClientConnection> clients;
    private final Map<String, Callback<Packet, Socket>> registry;
    private boolean running;
    private ServerSocket serverSocket;
    private Thread main;

    public ServerMain(int port) {
        this.port = port;
        this.idGenerator = new IDGenerator();
        clients = new HashSet<>();
        registry = new ConcurrentHashMap<>();
        runServer();
    }

    public boolean isRunning() {
        return running;
    }

    public Map<String, Callback<Packet, Socket>> getRegistry() {
        return registry;
    }

    public Set<ClientConnection> getClients() {
        return clients;
    }

    public void removeClient(ClientConnection con) {
        Predicate<ClientConnection> connectionPredicate = clientConnection -> clientConnection.getClientID().equalsIgnoreCase(con.getClientID());
        if(getClients().removeIf(connectionPredicate)) {
            con.stopClient();
        }
    }

    public void stopServer() {
        running = false;
        serverSocket = null;
        for (ClientConnection con : getClients()) {
            con.stopClient();
        }
        clients.clear();
        registry.clear();
        main.interrupt();
    }

    private void runServer() {
        main = new Thread(() -> {
            main.setUncaughtExceptionHandler(new PinSocketException());
            try {
                serverSocket = new ServerSocket(port);
                serverSocket.setReuseAddress(true);
                running = true;
                debug("Server started");
            } catch (IOException e) {
                debug("Startup failed...");
                e.printStackTrace();
            }
            while (running && !main.isInterrupted()) {
                try {
                    Socket socket = serverSocket.accept();
                    WhitelistIP whitelist = new WhitelistIP();
                    if (whitelist.load()) {
                        if (!whitelist.allowed(socket.getRemoteSocketAddress().toString())) {
                            Core.sendLogMessage("Could not verify IP " + socket.getRemoteSocketAddress() + " - client disconnected forcefully", false);
                            socket.close();
                        } else {
                            ClientConnection con = new ClientConnection(socket, this);
                            con.startClient();
                            clients.add(con);
                        }
                    } else {
                        ClientConnection con = new ClientConnection(socket, this);
                        con.startClient();
                        clients.add(con);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        main.start();
        register("net.client.login", ((packet, socket) -> {
            debug("New Client login [" + packet.signature + "@" + socket.getRemoteSocketAddress() + "]");
            ClientConnection con = cast(socket);
            if (con == null) {
                con = new ClientConnection(packet.signature, socket, this);
                clients.add(con);
                con.startClient();
            } else {
                if (con.getClientID().equalsIgnoreCase("NULL")) {
                    con.setClientID(packet.signature);
                }
            }
        }));
        register("net.client.respond.pong", (packet, socket) -> {
            ClientConnection con = cast(socket);
            sendMessage(con, new DefaultCallbackPacket());
            debug("Client " + con + " responded with pong.");
        });
        register("net.client.connection.disconnect.clean", (packet, socket) -> {
            ClientConnection con = cast(socket);
            debug("Client " + con + " disconnected cleanly");
            if (con != null && con.getSocket() != null)
                removeClient(con);
        });
    }

    public ClientConnection cast(Socket socket) {
        for (ClientConnection con : clients) {
            String localSocket = con.getSocket().getInetAddress().getHostAddress() + ":" + con.getSocket().getPort();
            String remoteSocket = socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
            if (localSocket.equals(remoteSocket)) {
                return con;
            }
        }
        return null;
    }

    public void broadcast(Packet packet) {
        debug("Sending packet to all clients");
        for (ClientConnection con : clients) {
            this.sendMessage(con, packet);
        }
    }

    private void sendMessage(ClientConnection receiver, Packet packet) {
        debug("Sending packet (" + packet.getId() + ") to " + receiver);
        if (!receiver.getSocket().isConnected()) {
            removeClient(receiver);
            debug("Client " + receiver + " disconnected");
            return;
        }
        receiver.sendMessage(packet);
    }

    public void register(String key, Callback<Packet, Socket> task) {
        debug("Registering key " + key);
        if (registry.containsKey(key))
            throw new MethodAlreadyRegisteredException();
        registry.put(key, task);
    }

    public IDGenerator getIdGenerator() {
        return idGenerator;
    }

    public void debug(String debug) {
        logger.info(debug);
        Core.sendLogMessage(debug, false);
    }
}
