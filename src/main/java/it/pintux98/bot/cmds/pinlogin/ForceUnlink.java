package it.pintux98.bot.cmds.pinlogin;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.CommandOption;
import it.pintux98.bot.cmds.PCommand;
import it.pintux98.bot.util.Utils;
import it.pintux98.server.db.entity.LinkedUser;
import it.pintux98.server.db.repository.LinkRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

public class ForceUnlink extends PCommand {

    @Autowired
    private LinkRepository linkRepository;

    public ForceUnlink() {
        this.name = "funlink";
        this.help = "Force player unlink";
        this.guildOnly = true;
        this.options = Collections.singletonList(
                new OptionData(OptionType.STRING, CommandOption.MC_NAME.getOption(), "Find player by minecraft name").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        List<LinkedUser> linkedUsers = Utils.getInstance().getLinkedUsers(event);

        if (linkedUsers.isEmpty()) {
            event.reply(Core.getFileManager().getLangData().getInvaliduser()).queue();
            return;
        }

        linkRepository.deleteAll(linkedUsers);

        event.replyEmbeds(new EmbedBuilder()
                .setTitle(Core.getFileManager().getGuildData().getBotsecurityprefix())
                .setDescription(Core.getFileManager().getLangData().getUnlinksuccess().replaceAll("%player%", "IDK"))
                .setColor(Color.RED)
                .setFooter("Author: pintux#2007")
                .setTimestamp(Instant.now())
                .build()).queue();
    }

    @Override
    public boolean isServerDependent() {
        return true;
    }

    @Override
    public String getPermission() {
        return "funlink";
    }
}
