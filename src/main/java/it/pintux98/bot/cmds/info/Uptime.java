package it.pintux98.bot.cmds.info;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.PCommand;
import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Instant;

public class Uptime extends PCommand {

    public Uptime() {
        this.name = "uptime";
        this.help = "Uptime for the bot";
    }

    @Override
    protected void execute(SlashCommandEvent event) {

        final RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        final long uptime = runtimeMXBean.getUptime();
        final long uptimeInSeconds = uptime / 1000L;
        final long numberOfHours = uptimeInSeconds / 3600L;
        final long numberOfMinutes = uptimeInSeconds / 60L - numberOfHours * 60L;
        final long numberOfSeconds = uptimeInSeconds % 60L;

        long restPing = event.getJDA().getRestPing().complete();

        String connected = Core.getInstance().getServer().isRunning() ? "CONNECTED" : "ERROR_CONNECTION";
        String db = "CONNECTED";

        event.replyEmbeds(new EmbedBuilder()
                .setColor(Color.GREEN)
                .setTitle("Info")
                .addField("Uptime: ", "`" + numberOfHours + "h " + numberOfMinutes + "m " + numberOfSeconds + "s`", false)
                .addField("Ping: ", "` WebSocket: " + restPing + "ms " + "\nServer: " + event.getJDA().getGatewayPing() + "ms `", false)
                .addField("Author: ", "`pintux#2007`", false)
                .addField("Server: ", "`" + connected + "`", false)
                .addField("Database:", "`" + db + "`", false)
                .setFooter("Author: pintux#2007")
                .setTimestamp(Instant.now())
                .build()).queue();
    }

    @Override
    public String getPermission() {
        return "uptime";
    }
}
