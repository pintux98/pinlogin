package it.pintux98.bot.util;

import it.pintux98.Core;
import it.pintux98.common.security.PSecurityException;
import it.pintux98.common.security.SecurityUtils;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.session.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class Listener extends ListenerAdapter {

    @Override
    public void onReady(@NotNull final ReadyEvent event) {
        Utils.getInstance().loadChannels();
        Core.getInstance().loadServerSocket();
        checkWhitelist();
    }

    private void checkWhitelist() {
        String s = Core.getJda().getToken().substring(0, 16);
        File inputFile = new File("PinServer/whitelist.txt");
        if (inputFile.exists() && !inputFile.isDirectory()) {
            File encryptedFile = new File("PinServer/stuff.end");
            try {
                SecurityUtils.encrypt(s, inputFile, encryptedFile);
            } catch (PSecurityException e) {
                Core.sendLogMessage(e.getMessage(), true);
                e.printStackTrace();
            }
        }
    }

    @Override
    public final void onMessageReactionAdd(final MessageReactionAddEvent event) {
        /*final MessageReaction emote = event.getReaction().getReactionEmote();
        if (event.getUser() != null && !event.getUser().isBot()) {
            if (event.getChannelType() == ChannelType.PRIVATE) {
                final Message message = event.getChannel().retrieveMessageById(event.getMessageId()).complete();
                if (!message.getAuthor().isBot()) {
                    return;
                }
                if (Core.getLoginData().containsKey(event.getUserIdLong())) {
                    LoginData data = Core.getLoginData().get(event.getUserIdLong());
                    FileManager fileManager = Core.getFileManager();
                    if (emote.getEmoji().equals("✅")) {
                        UserData userData = fileManager.getUserData(event.getUserIdLong());
                        List<String> logs = userData.getPinloginlogs();
                        logs.add("Login confirmation for mcName: " + data.getName() + " - Date: " + message.getTimeCreated() + " - IP: " + data.getIp());
                        userData.setPinloginlogs(logs);
                        userData.setUsingPinLogin(true);
                        fileManager.saveUserData(event.getUserIdLong(), userData);
                        message.delete().queue();

                        List<LinkedUser> linkedUsers = Core.getLinkManager().read(data.getName());

                        if (linkedUsers.isEmpty()) {
                            Core.getServer().broadcastPacket(new Packet(Core.id, Msg.M_LINK_NOT_LINKED.getCode(), data.getName()));
                            return;
                        }
                        for (LinkedUser linkedUser : linkedUsers) {
                            if (linkedUser.getName().equalsIgnoreCase(data.getName())) {
                                Core.getServer().broadcastPacket(new Packet(Core.id, Msg.M_LINK_ALLOW_LOGIN.getCode(), data.getName()));
                                linkedUser.setLastip(data.getIp());
                                Core.getLinkManager().update(linkedUser);

                                event.getChannel().sendMessage(new EmbedBuilder()
                                        .setTitle(Core.getFileManager().getGuildData().getBotsecurityprefix())
                                        .setDescription(Core.getFileManager().getLangData().getLoginaccept().replaceAll("%player%", data.getName()))
                                        .setColor(Color.GREEN)
                                        .setFooter("Author: pintux#2007")
                                        .setTimestamp(Instant.now())
                                        .build()).queue();

                                Core.getLoginData().remove(event.getUserIdLong());
                            }
                        }
                    } else if (emote.getEmoji().equals("⛔")) {
                        UserData userData = fileManager.getUserData(event.getUserIdLong());
                        List<String> logs = userData.getPinloginlogs();
                        logs.add("Login rejection for mcName: " + data.getName() + " - Date: " + message.getTimeCreated() + " - IP: " + data.getIp());
                        userData.setPinloginlogs(logs);
                        userData.setUsingPinLogin(true);
                        fileManager.saveUserData(event.getUserIdLong(), userData);
                        message.delete().queue();

                        List<LinkedUser> linkedUsers = Core.getLinkManager().read(data.getName());

                        if (linkedUsers.isEmpty()) {
                            Core.getServer().broadcastPacket(new Packet(Core.id, Msg.M_LINK_NOT_LINKED.getCode(), data.getName()));
                            return;
                        }
                        for (LinkedUser linkedUser : linkedUsers) {
                            if (linkedUser.getName().equalsIgnoreCase(data.getName())) {
                                Core.getServer().broadcastPacket(new Packet(Core.id, Msg.M_LINK_DENY_LOGIN.getCode(), data.getName()));
                                linkedUser.setLastip(data.getIp());
                                Core.getLinkManager().update(linkedUser);

                                event.getChannel().sendMessage(new EmbedBuilder()
                                        .setTitle(Core.getFileManager().getGuildData().getBotsecurityprefix())
                                        .setDescription(Core.getFileManager().getLangData().getLogindeny().replaceAll("%player%", data.getName()))
                                        .setColor(Color.GREEN)
                                        .setFooter("Author: pintux#2007")
                                        .setTimestamp(Instant.now())
                                        .build()).queue();
                                Core.getLoginData().remove(event.getUserIdLong());
                            }
                        }

                    }
                }
            }
        }

         */
    }
}
