package it.pintux98.server.db.entity;

public class LinkManager {

    /*
    private static final Logger logger = LogManager.getLogger(LinkManager.class.getName());

    private PinSQL sql;

    public LinkManager() {
        run();
    }

    private static void debug(String message) {
        Core.sendLogMessage(message, false);
        logger.info(message);
    }

    private void run() {
        debug("Loading database");
        sql = new PinSQL();
        if (sql.sqlSetup()) {
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    sql.ping();
                }
            }, 0, 60 * 1000);
        }
    }

    public boolean running() {
        return sql.sqlSetup();
    }

    public void create(LinkedUser person) {
        for (LinkedUser user : Core.getCachedUsers()) {
            if (user.getUuid().equals(person.getUuid())) {
                user.update(person);
            }
        }
        sql.addPlayer(person);
    }

    public LinkedUser readByCode(String code) {
        for (LinkedUser user : Core.getCachedUsers()) {
            if (user.getBackupCode().equalsIgnoreCase(code)) {
                return user;
            }
        }
        return sql.getPlayerByCode(code);
    }

    public List<LinkedUser> read(String value) {
        List<LinkedUser> users = new ArrayList<>();
        try {
            Long.parseLong(value);
            for (LinkedUser user : Core.getCachedUsers()) {
                if (user.getDiscordID().equals(value)) {
                    users.add(user);
                }
            }
        } catch (Exception ex) {
            for (LinkedUser user : Core.getCachedUsers()) {
                if (user.getName().equalsIgnoreCase(value)) {
                    users.add(user);
                }
            }
        }
        return users.isEmpty() ? sql.getPlayer(value) : users;
    }

    /*
    public List<LinkedUser> readAll(String disid) {
        return sql.getAccounts(disid);
    }



    public void update(LinkedUser person) {
        for (LinkedUser user : Core.getCachedUsers()) {
            if (user.getUuid().equals(person.getUuid())) {
                user.update(person);
            }
        }
        sql.updatePlayer(person);
    }

    public void delete(String name) {
        Core.getCachedUsers().removeIf(user -> user.getName().equalsIgnoreCase(name));
        sql.deletePlayer(name);
    }

    public void close() {
        sql.getHikari().close();
    }
         */
}
