package it.pintux98.server.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class PinGroup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String group;

    @OneToMany
    private Set<PinPermission> permissions;

    protected PinGroup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Set<PinPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<PinPermission> permissions) {
        this.permissions = permissions;
    }
}
