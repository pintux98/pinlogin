package it.pintux98.common;

import it.pintux98.Core;

import java.util.List;

public class FileHandler {

    public static String getMessage(String key) {
        return Core.getLang().getString(key);
    }

    public static List<String> getMessageList(String key) {
        return (List<String>) Core.getLang().getList(key);
    }

    public static long getLongValue(String key) {
        return Core.getConfig().getLong(key);
    }

    public static String getStringValue(String key) {
        return Core.getConfig().getString(key);
    }

    public static int getIntValue(String key) {
        return Core.getConfig().getInt(key);
    }
}
