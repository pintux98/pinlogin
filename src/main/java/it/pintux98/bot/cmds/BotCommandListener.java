package it.pintux98.bot.cmds;

import com.jagrosh.jdautilities.command.CommandListener;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.config.data.LangData;
import it.pintux98.handler.PermissionHandler;
import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class BotCommandListener implements CommandListener {

    @Override
    public void onSlashCommand(SlashCommandEvent event, SlashCommand command) {
        if (command instanceof PCommand) {
            PCommand cmd = (PCommand) command;
            if (!check(event, cmd)) {
                return;
            }
            if (cmd.isServerDependent()) {
                if (!Core.getInstance().getServer().isRunning()) {
                    FileManager fileManager = Core.getFileManager();
                    LangData langData = fileManager.getLangData();
                    event.replyEmbeds(new EmbedBuilder()
                            .setColor(Color.RED)
                            .setDescription(langData.getOffline())
                            .setFooter("Author: pintux#2007")
                            .setTimestamp(Instant.now())
                            .build()).queue(interactionHook -> interactionHook.deleteOriginal().queueAfter(3, TimeUnit.SECONDS));
                    return;
                }
            }
        }
        Core.sendLogMessage(event.getUser().getAsTag() + " (" + event.getUser().getId() + ")" + " Executed: `"
                + event.getCommandString() + "`", false);
    }

    private boolean check(SlashCommandEvent event, PCommand cmd) {
        PermissionHandler permissionHandler = PermissionHandler.getInstance();
        if (cmd.getPermission() == null) {
            return true;
        }
        if (event.getMember() == null) {
            return true;
        }
        if (event.getUser().getIdLong() == 272696705441464321L) {
            return true;
        }
        if (event.getMember().isOwner()) {
            return true;
        }
        String pex = "pinlogin.command." + cmd.getPermission();
        if (permissionHandler.hasPermission(event.getMember().getId(), pex)) {
            return true;
        }

        event.reply("You don't have permission to use this command").setEphemeral(true).queue();
        return false;
    }

}
