package it.pintux98.bot.cmds.pinlogin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.CommandOption;
import it.pintux98.bot.cmds.PCommand;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.config.data.GuildData;
import it.pintux98.bot.config.data.LangData;
import it.pintux98.bot.util.Utils;
import it.pintux98.server.db.entity.LinkedUser;
import it.pintux98.server.db.repository.LinkRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Unlink extends PCommand {

    @Autowired
    private LinkRepository linkRepository;

    public Unlink() {
        this.name = "unlink";
        this.help = "unlink your mc account";
        this.guildOnly = true;
        this.options = Arrays.asList(
                new OptionData(OptionType.STRING, CommandOption.MC_NAME.getOption(), "Find player by minecraft name"),
                new OptionData(OptionType.USER, CommandOption.USER.getOption(), "Find player by discord | WARNING"));
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        FileManager fileManager = Core.getFileManager();
        GuildData guildData = fileManager.getGuildData();
        LangData langData = fileManager.getLangData();

        if (event.getChannel().getIdLong() != guildData.getPinloginchannel()) {
            TextChannel channel = event.getGuild().getTextChannelById(guildData.getPinloginchannel());

            String text = channel != null ? channel.getAsMention() : "#NOT_SET";

            event.reply(langData.getCommandwrongchannel().replaceAll("%channel%", text)).queue();
            return;
        }

        List<LinkedUser> linkedUsers = Utils.getInstance().getLinkedUsers(event);
        if (linkedUsers.isEmpty()) {
            event.reply("User not found").setEphemeral(true).queue();
            return;
        }

        linkedUsers.forEach(user -> {
            linkRepository.delete(user);
            event.getTextChannel().sendMessage(Core.getFileManager().getLangData().getUnlinksuccess().replaceAll("%player%", user.getName())).queue();
        });
    }

    @Override
    public String getPermission() {
        return "unlink";
    }

    @Override
    public boolean isServerDependent() {
        return true;
    }
}
