package it.pintux98.common.security;

import it.pintux98.Core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WhitelistIP {

    private final List<String> whitelist;

    public WhitelistIP() {
        whitelist = new ArrayList<>();
    }

    public boolean load() {
        File input = new File("PinServer/stuff.end");
        File output;
        try {
            output = File.createTempFile("PinServer/ips", ".ded");
        } catch (IOException e) {
            Core.sendLogMessage(e.getMessage(), false);
            e.printStackTrace();
            return false;
        }
        if (input.exists() && !input.isDirectory()) {
            try {
                SecurityUtils.decrypt(Core.getJda().getToken().substring(0, 16), input, output);
            } catch (PSecurityException e) {
                Core.sendLogMessage(e.getMessage(), true);
                e.printStackTrace();
                return false;
            }
            if (output.exists() && !output.isDirectory()) {
                try {
                    Scanner scanner = new Scanner(output);
                    while (scanner.hasNextLine()) {
                        whitelist.add(scanner.nextLine());
                    }
                    scanner.close();
                } catch (FileNotFoundException e) {
                    Core.sendLogMessage(e.getMessage(), true);
                    e.printStackTrace();
                    return false;
                }
                return output.delete();
            }
        }
        return true;
    }

    public boolean allowed(String ip) {
        String cleaned = ip.split(":")[0].replaceAll("/", "");
        for (String saved : whitelist) {
            if (saved.equalsIgnoreCase(cleaned)) {
                return true;
            }
        }
        return false;
    }
}
