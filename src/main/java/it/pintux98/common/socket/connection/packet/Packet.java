package it.pintux98.common.socket.connection.packet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Packet extends ArrayList<Object> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5305919933485806971L;
    private final String id;
    public String signature;

    /**
     * Creates a new packet with the specified id
     * and no content.
     *
     * @param id packet id
     */
    public Packet(String id) {
        this(id, new Object[0]);
    }

    /**
     * Creates a new packet with the specified id
     * and content.
     *
     * @param id   packet id
     * @param objs content
     */
    public Packet(String id, Object... objs) {
        this.id = id;
        addAll(Arrays.asList(objs));
    }

    /**
     * @return the packet's id
     */
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "id='" + id + '\'' +
                ", signature='" + signature + '\'' +
                ", modCount=" + modCount + '\'' +
                ", content=" + Arrays.toString(this.toArray()) +
                '}';
    }
}
