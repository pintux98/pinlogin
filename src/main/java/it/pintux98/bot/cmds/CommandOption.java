package it.pintux98.bot.cmds;

public enum CommandOption {

    CODE("code"),
    MC_NAME("mcname"),
    USER("useroption"),
    CHANNEL("channeloption");

    private String option;

    CommandOption(String option) {
        this.option = option;
    }

    public String getOption() {
        return option;
    }
}
