package it.pintux98.server.db.repository;

import it.pintux98.server.db.entity.PinPermission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PermissionRepository extends CrudRepository<PinPermission, Long> {

    @Query(value = "select pex from PinPermission pex where pex.permission=:permission")
    PinPermission findFirstByPermission(String permission);

}
