package it.pintux98.server.db.repository;

import it.pintux98.server.db.entity.LinkedUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LinkRepository extends CrudRepository<LinkedUser, Long> {

    List<LinkedUser> findLinkedUserByName(String name);
    LinkedUser findLinkedUserByBackupCode(String backupCode);
    List<LinkedUser> findLinkedUsersByDiscordID(String discordID);

}
