package it.pintux98.common.security;

public class LoginData {

    private final String name;
    private final String ip;

    public LoginData(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }
}
