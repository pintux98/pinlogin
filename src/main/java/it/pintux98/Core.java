package it.pintux98;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import it.pintux98.bot.cmds.BotCommandListener;
import it.pintux98.bot.cmds.admin.Shutdown;
import it.pintux98.bot.cmds.admin.*;
import it.pintux98.bot.cmds.info.Uptime;
import it.pintux98.bot.cmds.pinlogin.ForceUnlink;
import it.pintux98.bot.cmds.pinlogin.Link;
import it.pintux98.bot.cmds.pinlogin.Unlink;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.util.Listener;
import it.pintux98.bot.util.Utils;
import it.pintux98.common.file.ConfigFile;
import it.pintux98.common.file.LangFile;
import it.pintux98.common.security.CheckMode;
import it.pintux98.common.security.IPUtils;
import it.pintux98.common.security.LoginData;
import it.pintux98.common.socket.PinClient;
import it.pintux98.common.socket.PinServer;
import it.pintux98.common.socket.connection.packet.Packet;
import it.pintux98.common.socket.impl.PinServerSocketImpl;
import it.pintux98.server.Msg;
import it.pintux98.server.db.entity.LinkedUser;
import it.pintux98.server.db.repository.LinkRepository;
import it.pintux98.server.db.repository.LoginRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@SpringBootApplication
public class Core {

    public final static String id = "it.pintux98.pinlogin.minecraft";
    private static final Logger logger = LogManager.getLogger("PinLoginPremium");
    private static List<LinkedUser> cachedUsers;
    private static ConcurrentHashMap<Long, LoginData> loginData;
    private static FileManager fileManager;
    private static JDA jda;
    private static ConfigFile config;
    private static LangFile lang;
    private static PinServer server;
    private static IPUtils ipUtils;

    private static Core instance;

    public static Core getInstance() {
        if (instance == null) {
            instance = new Core();
        }
        return instance;
    }

    @Autowired
    private LinkRepository linkRepository;

    @Autowired
    private LoginRepository loginRepository;

    public Core() {
        instance = this;
        loadFiles();
        EventWaiter waiter = new EventWaiter();
        setFileManager(new FileManager());
        CommandClientBuilder playerCommands = new CommandClientBuilder()
                .setPrefix(fileManager.getGuildData().getCmduserprefix())
                .setOwnerId("272696705441464321")
                .setEmojis("✅", "⚠", "⛔")
                .useHelpBuilder(false)
                .setListener(new BotCommandListener())
                .addSlashCommands(
                        new Player(),
                        new Clients(),
                        new Restart(),
                        new Shutdown(),
                        new Uptime(),
                        new Link(),
                        new ForceUnlink(),
                        new Unlink()
                )
                .addCommands(
                        new Settings()
                ).forceGuildOnly(1033791462313304234L);
        JDABuilder builder = JDABuilder.createDefault("NzE0OTU3NDgzNTM3NTk2NDg2.GmgxVD.9wHZ4EmaiLefX5-uvyYUPlCqkadNPdJZkssS0w")
                .addEventListeners(waiter, playerCommands.build())
                .addEventListeners(new Listener());
        configureMemoryUsage(builder);
        try {
            jda = builder.build();
        } catch (Exception e) {
            logger.info("Error ... please insert in PinLoginPremium/guild/guild.yml a valid BOT_TOKEN");
            System.exit(0);
        }
        loginData = new ConcurrentHashMap<>();
    }

    public static IPUtils getIpUtils() {
        return ipUtils;
    }

    public static List<LinkedUser> getCachedUsers() {
        return cachedUsers;
    }

    public static ConfigFile getConfig() {
        return config;
    }

    public static LangFile getLang() {
        return lang;
    }

    public static JDA getJda() {
        return jda;
    }


    public static void main(String[] args) {
        SpringApplication.run(Core.class, args);
    }

    public static Logger getLogger() {
        return logger;
    }

    public static ConcurrentHashMap<Long, LoginData> getLoginData() {
        return loginData;
    }

    public static FileManager getFileManager() {
        return fileManager;
    }

    public static void setFileManager(FileManager fileManager) {
        Core.fileManager = fileManager;
    }

    public PinServer getServer() {
        return server;
    }

    public void loadServerSocket() {

        debug("Loading server socket");
        server = new PinServerSocketImpl(config.getInt("ServerPort"));

        server.registerPacket(id, (packet, socket) -> {
            debug("Received PinLogin Minecraft packet [" + packet.getId() + " - " + socket.getRemoteSocketAddress() + "]");
            handlePacket(packet, socket);
        });
    }

    public static void debug(String message) {
        logger.info(message);
    }

    private void handlePacket(Packet received, Socket clientSocket) {

        PinClient client = getServer().castFromSocket(clientSocket);

        if (client == null) {
            debug("Invalid client tried to send packet from " + clientSocket.getRemoteSocketAddress());
            return;
        }

        Object[] arr = received.toArray();

        int code = (int) arr[0];

        String uuid = String.valueOf(arr[1]);
        String name = String.valueOf(arr[2]);
        InetSocketAddress address = (InetSocketAddress) arr[3];
        String ip = address.getAddress().getHostAddress();

        List<LinkedUser> linkedUsers;


        switch (code) {
            case 200: //Check login
                linkedUsers = linkRepository.findLinkedUserByName(name);

                if (linkedUsers.isEmpty()) {
                    client.sendPacket(new Packet(id, Msg.M_LINK_NOT_LINKED.getCode(), name));
                    return;
                }

                linkedUsers.stream().filter(linkedUser -> linkedUser.getName().equalsIgnoreCase(name))
                        .findFirst().ifPresent(linkedUser -> {
                            if (!linkedUser.isLinked()) {
                                client.sendPacket(new Packet(id, Msg.M_LINK_NOT_LINKED_CODE.getCode(), name, linkedUser.getBackupCode()));
                                return;
                            }

                            if (getIpUtils().check(linkedUser, ip)) {
                                client.sendPacket(new Packet(id, Msg.M_LINK_SUCCESS_LOGIN_IP.getCode(), name));
                                return;
                            }
                            client.sendPacket(new Packet(id, Msg.M_LINK_NEW_LOGIN.getCode(), name));

                            User user = getJda().getUserById(linkedUser.getDiscordID());
                            if (user == null) {
                                return;
                            }

                            if (Core.getLoginData().get(user.getIdLong()) != null) {
                                Core.getLoginData().remove(user.getIdLong());
                            }

                            Core.getLoginData().put(user.getIdLong(), new LoginData(name, ip));

                            user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessageEmbeds(new EmbedBuilder()
                                    .setTitle(fileManager.getGuildData().getBotsecurityprefix())
                                    .setDescription(fileManager.getLangData().getLoginalert().replaceAll("%player%", "`" + name + "`").replaceAll("%ip%", "`" + ip + "`"))
                                    .setColor(Color.GREEN)
                                    .setFooter("Author: pintux#2007")
                                    .setTimestamp(Instant.now())
                                    .build()).queue(message -> {
                                message.addReaction(Emoji.fromUnicode(Utils.CHECK_EMOJI)).queue();
                                message.addReaction(Emoji.fromUnicode(Utils.DENY_EMOJI)).queue();
                            }));
                        });
                break;
            case 201: //Check link

                String secret = (String) arr[4];
                linkedUsers = linkRepository.findLinkedUserByName(name);

                if (!linkedUsers.isEmpty()) {
                    for (LinkedUser linkedUser : linkedUsers) {
                        if (linkedUser.getName().equalsIgnoreCase(name)) {
                            client.sendPacket(new Packet(id, Msg.M_LINK_NOT_LINKED_CODE.getCode(), name, linkedUser.getBackupCode()));
                            break;
                        }
                    }
                    return;
                }

                LinkedUser linkedUser = new LinkedUser(name, uuid, secret);

                cachedUsers.add(linkedUser);

                break;
            default:
                break;
        }
    }

    public static void sendLogMessage(String message, boolean error) {
        if (jda != null) {
            TextChannel logs = jda.getTextChannelById(793802592739786752L);
            if (logs != null) {
                String title = error ? "ERROR" : "INFO";
                User user = jda.getUserById(272696705441464321L);
                logs.sendMessage((error ? (user != null ? user.getAsMention() : "") : "") + " [" + title + "] " + (message.length() > 2000 ? message.substring(0, 2000) : message)).queue();
            }
        }
        debug(message);
    }

    private void loadFiles() {
        config = new ConfigFile();
        lang = new LangFile();
        ipUtils = new IPUtils(CheckMode.IMPROVED);
        cachedUsers = new ArrayList<>();
    }

    public void configureMemoryUsage(JDABuilder builder) {
        builder.disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.CLIENT_STATUS);
        builder.setMemberCachePolicy(MemberCachePolicy.ALL);
        builder.setChunkingFilter(ChunkingFilter.ALL);
        builder.enableIntents(GatewayIntent.GUILD_MEMBERS, GatewayIntent.MESSAGE_CONTENT);
        builder.disableIntents(GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_MESSAGE_TYPING, GatewayIntent.GUILD_INVITES, GatewayIntent.GUILD_VOICE_STATES, GatewayIntent.DIRECT_MESSAGE_TYPING);
        builder.setLargeThreshold(5000);
    }
}
