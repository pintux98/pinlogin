package it.pintux98.bot.cmds.admin;

import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.CommandOption;
import it.pintux98.bot.cmds.PCommand;
import it.pintux98.bot.util.Utils;
import it.pintux98.server.db.entity.LoginSession;
import it.pintux98.server.db.repository.LinkRepository;
import it.pintux98.server.db.entity.LinkedUser;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Player extends PCommand {

    @Autowired
    private LinkRepository linkRepository;

    public Player() {
        this.name = "player";
        this.help = "Find player information";
        this.guildOnly = true;
        this.children = new SlashCommand[]{new Find()};
    }

    @Override
    protected void execute(SlashCommandEvent event) {
    }

    @Override
    public String getPermission() {
        return "player";
    }

    @Override
    public boolean isServerDependent() {
        return true;
    }

    public class Find extends PCommand {

        public Find() {
            this.name = "find";
            this.help = "Find player information";
            this.guildOnly = true;
            this.options = Arrays.asList(
                    new OptionData(OptionType.STRING, CommandOption.MC_NAME.getOption(), "Find player by minecraft name"),
                    new OptionData(OptionType.USER, CommandOption.USER.getOption(), "Find player by discord"));
        }

        @Override
        protected void execute(SlashCommandEvent event) {

            List<LinkedUser> linkedUsers = Utils.getInstance().getLinkedUsers(event);

            if (linkedUsers.isEmpty()) {
                event.reply(Core.getFileManager().getLangData().getInvaliduser()).queue();
                return;
            }

            linkedUsers.forEach(linkedUser -> {
                String disTag = "No member found on the server!";
                User u = Core.getJda().getUserById(linkedUser.getDiscordID());
                if (u != null) {
                    disTag = u.getAsTag();
                }
                Set<LoginSession> sessions = linkedUser.getLoginSessions();
                StringBuilder sb = new StringBuilder();
                sb.append("Login Sessions for( ").append(linkedUser.getName()).append(" ): ");
                sessions.stream().limit(5).forEach(loginSession ->
                        sb.append("IP: ").append(loginSession.getLoginSessionAddress().getAddress()).append("\n").append("Date: ").append(loginSession.getLoginTime().toLocalDateTime()));
                event.getTextChannel().sendMessageEmbeds(new EmbedBuilder()
                        .setTitle(Core.getFileManager().getGuildData().getBotsecurityprefix())
                        .setDescription(Core.getFileManager().getLangData().getUserinfo()
                                .replaceAll("%player%", linkedUser.getName())
                                .replaceAll("%curip%", "IDK")
                                .replaceAll("%disID%", linkedUser.getDiscordID())
                                .replaceAll("%distag%", disTag))
                        .setColor(Color.GREEN)
                        .setFooter("Author: pintux#2007")
                        .setTimestamp(Instant.now())
                        .build()).queue();
                event.getTextChannel().sendMessage(sb.toString()).queue();
            });
        }

        @Override
        public String getPermission() {
            return "find";
        }

        @Override
        public boolean isServerDependent() {
            return true;
        }
    }
}
