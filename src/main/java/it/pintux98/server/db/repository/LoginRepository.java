package it.pintux98.server.db.repository;

import it.pintux98.server.db.entity.LoginSession;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LoginRepository extends CrudRepository<LoginSession, Long> {

    List<LoginSession> findLoginSessionsById(Long id);
    
}
