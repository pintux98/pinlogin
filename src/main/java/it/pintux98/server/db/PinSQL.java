package it.pintux98.server.db;

import com.zaxxer.hikari.HikariDataSource;
import it.pintux98.Core;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PinSQL {

    /*
    private final Logger logger = LogManager.getLogger("PinSQL");
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    private Connection con;
    private String host, database, user, pass, table;
    private int port;
    private HikariDataSource hikari;

    public boolean sqlSetup() {
        try {
            synchronized (PinSQL.class) {
                if (getHikari() != null && getCon() != null && !getCon().isClosed())
                    return true;
                loadAccessData();
                setHikari(new HikariDataSource());
                getHikari().setMaximumPoolSize(10);
                getHikari().setDataSourceClassName("org.mariadb.jdbc.MariaDbDataSource");
                getHikari().addDataSourceProperty("serverName", getHost());
                getHikari().addDataSourceProperty("port", getPort());
                getHikari().addDataSourceProperty("databaseName", getDatabase());
                getHikari().addDataSourceProperty("user", getUser());
                getHikari().addDataSourceProperty("password", getPass());
                statement = getHikari().getConnection().createStatement();
                statement.executeUpdate("USE " + getDatabase());
                setCon(getHikari().getConnection());
                createTable();
                logger.info("DATABASE CONNECTED");
            }
        } catch (SQLException e) {
            logger.info("AN ERROR OCURED WHILE TRYING TO CONNECT TO DATABASE");
            Core.sendLogMessage(e.getMessage(), true);
            return false;
        }
        return true;
    }

    private void loadAccessData() {
        setHost(Core.getConfig().getString("hostname"));
        setDatabase(Core.getConfig().getString("database"));
        setUser(Core.getConfig().getString("username"));
        setPass(Core.getConfig().getString("password"));
        setPort(Core.getConfig().getInt("port"));
        setTable(Core.getConfig().getString("table"));
        setTable("player");
    }

    private void createTable() {

        String pinloginTable = "CREATE TABLE IF NOT EXISTS " + getTable() + " (uuid varchar(36) NOT NULL PRIMARY KEY, name varchar(16) NOT NULL, backupCode varchar(16) NOT NULL, discordID varchar(26) NOT NULL, lastIP varchar(18) NOT NULL, linked BIT NOT NULL, created TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE=INNODB";
        String logsTable = "CREATE TABLE IF NOT EXISTS " + getTable().concat("logs") + " (ID AUTO_INCREMENT, discordID varchar(26) NOT NULL, name varchar(16) NOT NULL, lastIP varchar(18) NOT NULL, logged TIMESTAMP DEFAULT CURRENT_TIMESTAMP, FOREIGN KEY (discordID) REFERENCES " + getTable() + " (discordID) ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=INNODB";
        try {
            statement = getHikari().getConnection().createStatement();
            statement.executeUpdate(pinloginTable);
            statement.executeUpdate(logsTable);
        } catch (SQLException ex) {
            Core.sendLogMessage(ex.getMessage(), true);
            ex.printStackTrace();
        }
    }

    public void ping() {
        try {
            if (sqlSetup()) {
                preparedStatement = getCon().prepareStatement("SELECT 1");
                preparedStatement.executeQuery();
                preparedStatement.closeOnCompletion();
            }
        } catch (SQLException e) {
            Core.sendLogMessage(e.getMessage(), true);
            e.printStackTrace();
        }
    }

    public List<String> getLogs(String value) {
        List<String> logs = new ArrayList<>();
        try {
            try {
                Long.parseLong(value);
                preparedStatement = getCon()
                        .prepareStatement("SELECT * FROM " + getTable().concat("logs") + " WHERE discordID='" + value + "'");
            } catch (Exception e) {
                preparedStatement = getCon()
                        .prepareStatement("SELECT * FROM " + getTable().concat("logs") + " WHERE name='" + value + "'");
            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String discordID = resultSet.getString("discordID");
                String name = resultSet.getString("name");
                String lastIP = resultSet.getString("lastIP");
                Timestamp logged = resultSet.getTimestamp("logged");
                logs.add("(" + discordID + ") " + " name " + name + " logged on " + logged + " with IP " + lastIP);
            }
            return logs;
        } catch (SQLException e) {
            e.printStackTrace();
            Core.sendLogMessage(e.getMessage(), true);
        }
        return logs;
    }

    public void addLog(LinkedUser user) {
        try {
            if (sqlSetup()) {
                preparedStatement = getCon().prepareStatement("INSERT INTO " + getTable().concat("logs")
                        + " (discordID, name, lastIP) VALUES(?,?,?)");
                preparedStatement.setString(1, user.getDiscordID());
                preparedStatement.setString(2, user.getName());
                preparedStatement.setString(3, user.getLastip());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Core.sendLogMessage(e.getMessage(), true);
        }
    }

    public LinkedUser getPlayerByCode(String code) {
        try {
            if (sqlSetup()) {
                preparedStatement = getCon()
                        .prepareStatement("SELECT * FROM " + getTable() + " WHERE backupCode='" + code + "'");
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    String uuid = resultSet.getString("uuid");
                    String name = resultSet.getString("name");
                    String discordID = resultSet.getString("discordID");
                    String lastip = resultSet.getString("lastIP");
                    boolean linked = resultSet.getBoolean("linked");
                    Timestamp created = resultSet.getTimestamp("created");

                    LinkedUser user = new LinkedUser();
                    user.setUuid(uuid);
                    user.setName(name);
                    user.setLastip(lastip);
                    user.setLinked(linked);
                    user.setBackupCode(code);
                    user.setDiscordID(discordID);
                    user.setCreated(created);
                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Core.sendLogMessage(e.getMessage(), true);
        }
        return null;
    }

    public List<LinkedUser> getPlayer(String value) {
        List<LinkedUser> users = new ArrayList<>();
        try {
            if (sqlSetup()) {
                try {
                    Long.parseLong(value);
                    preparedStatement = getCon()
                            .prepareStatement("SELECT * FROM " + getTable() + " WHERE discordID='" + value + "'");
                } catch (Exception e) {
                    preparedStatement = getCon()
                            .prepareStatement("SELECT * FROM " + getTable() + " WHERE name='" + value + "'");
                }
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    String uuid = resultSet.getString("uuid");
                    String name = resultSet.getString("name");
                    String backupCode = resultSet.getString("backupCode");
                    String discordID = resultSet.getString("discordID");
                    String lastip = resultSet.getString("lastIP");
                    boolean linked = resultSet.getBoolean("linked");
                    Timestamp created = resultSet.getTimestamp("created");

                    LinkedUser user = new LinkedUser();
                    user.setUuid(uuid);
                    user.setName(name);
                    user.setLastip(lastip);
                    user.setLinked(linked);
                    user.setBackupCode(backupCode);
                    user.setDiscordID(discordID);
                    user.setCreated(created);
                    users.add(user);
                }
                return users;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
    public void addPlayer(LinkedUser link) {
        try {
            if (sqlSetup()) {
                preparedStatement = getCon().prepareStatement("INSERT INTO " + getTable()
                        + " (uuid, name, discordID, lastIP, backupCode, linked, created) VALUES(?,?,?,?,?,?,?)");
                preparedStatement.setString(1, link.getUuid());
                preparedStatement.setString(2, link.getName());
                preparedStatement.setString(3, link.getDiscordID());
                preparedStatement.setString(4, link.getLastip());
                preparedStatement.setString(5, link.getBackupCode());
                preparedStatement.setBoolean(6, link.isLinked());
                preparedStatement.setTimestamp(7, link.getCreated());
                preparedStatement.executeUpdate();
                logger.info("Created new user " + link.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Unable to add user to database");
            Core.sendLogMessage(e.getMessage(), true);
        }
    }

    public void updatePlayer(LinkedUser link) {
        try {
            if (sqlSetup()) {
                if (getPlayer(link.getName()) == null) {
                    addPlayer(link);
                    return;
                }
                preparedStatement = getCon().prepareStatement(
                        "UPDATE " + getTable() + " SET name=?,discordID=?,lastIP=?,backupCode=?,linked=? WHERE uuid=?");
                preparedStatement.setString(1, link.getName());
                preparedStatement.setString(2, link.getDiscordID());
                preparedStatement.setString(3, link.getLastip());
                preparedStatement.setString(4, link.getBackupCode());
                preparedStatement.setBoolean(5, link.isLinked());
                preparedStatement.setString(6, link.getUuid());
                preparedStatement.executeUpdate();
                logger.info("Updated user " + link.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("Unable to update user in database...");
            Core.sendLogMessage(e.getMessage(), true);
        }
    }

    public void deletePlayer(String value) {
        try {
            if (sqlSetup()) {
                preparedStatement = getCon().prepareStatement(
                        "DELETE FROM " + getTable() + " WHERE name=?");
                preparedStatement.setString(1, String.valueOf(value));
                preparedStatement.executeUpdate();
                logger.info("Deleted user with name " + value);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Core.sendLogMessage(e.getMessage(), true);
            logger.info("Unable to delete user from database");
        }
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public HikariDataSource getHikari() {
        return hikari;
    }

    public void setHikari(HikariDataSource hikari) {
        this.hikari = hikari;
    }

    private LocalDateTime getLocalDateTime(Date date) {
        return date.toLocalDate().atStartOfDay();
    }

    private Date getDate(LocalDateTime dateToConvert) {
        return Date.valueOf(dateToConvert.toLocalDate());
    }

     */
}