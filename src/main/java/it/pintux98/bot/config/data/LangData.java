package it.pintux98.bot.config.data;

public class LangData {

    private String pinlogin;
    private String offline;
    private String commandwrongchannel;
    private String linkcode;
    private String linklinked;
    private String unlinksuccess;
    private String unlinkfaildiscord;
    private String unlinkfailname;
    private String unlinkfailunknown;
    private String loginaccept;
    private String logindeny;
    private String loginalert;
    private String invaliduser;
    private String userinfo;
    private String cantsenddm;

    public LangData() {
    }

    public String getCommandwrongchannel() {
        return commandwrongchannel;
    }

    public void setCommandwrongchannel(String commandwrongchannel) {
        this.commandwrongchannel = commandwrongchannel;
    }

    public String getPinlogin() {
        return pinlogin;
    }

    public void setPinlogin(String pinlogin) {
        this.pinlogin = pinlogin;
    }

    public String getOffline() {
        return offline;
    }

    public void setOffline(String offline) {
        this.offline = offline;
    }

    public String getCantsenddm() {
        return cantsenddm;
    }

    public void setCantsenddm(String cantsenddm) {
        this.cantsenddm = cantsenddm;
    }

    public String getLinkcode() {
        return linkcode;
    }

    public void setLinkcode(String linkcode) {
        this.linkcode = linkcode;
    }

    public String getLinklinked() {
        return linklinked;
    }

    public void setLinklinked(String linklinked) {
        this.linklinked = linklinked;
    }

    public String getUnlinksuccess() {
        return unlinksuccess;
    }

    public void setUnlinksuccess(String unlinksuccess) {
        this.unlinksuccess = unlinksuccess;
    }

    public String getUnlinkfaildiscord() {
        return unlinkfaildiscord;
    }

    public void setUnlinkfaildiscord(String unlinkfaildiscord) {
        this.unlinkfaildiscord = unlinkfaildiscord;
    }

    public String getUnlinkfailname() {
        return unlinkfailname;
    }

    public void setUnlinkfailname(String unlinkfailname) {
        this.unlinkfailname = unlinkfailname;
    }

    public String getUnlinkfailunknown() {
        return unlinkfailunknown;
    }

    public void setUnlinkfailunknown(String unlinkfailunknown) {
        this.unlinkfailunknown = unlinkfailunknown;
    }

    public String getLoginaccept() {
        return loginaccept;
    }

    public void setLoginaccept(String loginaccept) {
        this.loginaccept = loginaccept;
    }

    public String getLogindeny() {
        return logindeny;
    }

    public void setLogindeny(String logindeny) {
        this.logindeny = logindeny;
    }

    public String getLoginalert() {
        return loginalert;
    }

    public void setLoginalert(String loginalert) {
        this.loginalert = loginalert;
    }

    public String getInvaliduser() {
        return invaliduser;
    }

    public void setInvaliduser(String invaliduser) {
        this.invaliduser = invaliduser;
    }

    public String getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(String userinfo) {
        this.userinfo = userinfo;
    }

    @Override
    public String toString() {
        return "LangData{" +
                "pinlogin='" + pinlogin + '\'' +
                ", commandwrongchannel='" + commandwrongchannel + '\'' +
                ", offline='" + offline + '\'' +
                ", cantsenddm='" + cantsenddm + '\'' +
                ", linkcode='" + linkcode + '\'' +
                ", linklinked='" + linklinked + '\'' +
                ", unlinksuccess='" + unlinksuccess + '\'' +
                ", unlinkfaildiscord='" + unlinkfaildiscord + '\'' +
                ", unlinkfailname='" + unlinkfailname + '\'' +
                ", unlinkfailunknown='" + unlinkfailunknown + '\'' +
                ", loginaccept='" + loginaccept + '\'' +
                ", logindeny='" + logindeny + '\'' +
                ", loginalert='" + loginalert + '\'' +
                ", invaliduser='" + invaliduser + '\'' +
                ", userinfo='" + userinfo + '\'' +
                '}';
    }
}
