package it.pintux98.common.socket.impl;

import it.pintux98.Core;
import it.pintux98.common.socket.PinClient;
import it.pintux98.common.socket.PinServer;
import it.pintux98.common.socket.connection.packet.Packet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class PinClientSocketImpl implements PinClient {


    private final Logger logger = LogManager.getLogger("PinClientSocketImpl");
    private final Socket socket;
    private final String serverID;
    private final PinServer server;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String clientID;
    private Thread thread;

    public PinClientSocketImpl(String clientID, PinServer server, Socket socket1){
        this.server = server;
        this.socket = socket1;
        this.clientID = clientID;
        this.serverID = "";
    }

    public PinClientSocketImpl(PinServer server, Socket socket){
        this.server = server;
        this.clientID = "NULL";
        this.socket = socket;
        this.serverID = "";
    }

    @Override
    public String getClientID() {
        return clientID;
    }

    @Override
    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    @Override
    public String getServerID() {
        return serverID;
    }

    @Override
    public Socket getSocket() {
        return socket;
    }

    @Override
    public void startClient() {
        thread = new Thread(() -> {
                try {
                    output = new ObjectOutputStream(socket.getOutputStream());
                    input = new ObjectInputStream(socket.getInputStream());
                    Object obj = input.readObject();
                    if (obj instanceof Packet) {
                        Packet packet = (Packet) obj;
                        debug("Packet received (" + packet.getId() + ") " + packet);
                        for (String reg : server.getRegistry().keySet()) {
                            if (packet.getId().equals(reg)) {
                                new Thread(() -> {
                                    try {
                                        server.getRegistry().get(reg).respond(packet, socket);
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                }).start();
                            }
                        }
                    } else {
                        debug("Received unknown object " + obj);
                    }
                } catch (IOException | ClassNotFoundException e) {
                    debug("I/O Exception in reading packet");
                    Core.sendLogMessage(e.getMessage(), true);
                    //e.printStackTrace();
                    server.removeClient(this);
                }
        });
        thread.start();
    }

    @Override
    public void stopClient() {
        debug("Closing client " + serverID + "-" + clientID);
        try {
            if (socket != null) {
                socket.close();
            }
            if (input != null) {
                input.close();
            }

            if (output != null) {
                output.close();
            }
            thread.interrupt();
        } catch (IOException e) {
            debug("Close exception");
            e.printStackTrace();
        }
    }

    @Override
    public void sendPacket(Packet packet) {
        debug("Sending packet (" + packet.getId() + ") to " + socket);
        if (!socket.isConnected()) {
            server.removeClient(this);
            debug("Client " + socket + " disconnected");
            return;
        }
        try {
            output.writeObject(packet);
            output.flush();
        } catch (IOException e) {
            server.removeClient(this);
            debug("Client " + socket + " disconnected");
        }
    }

    public void debug(String debug) {
        Core.sendLogMessage(debug, false);
        logger.info(debug);
    }
}
