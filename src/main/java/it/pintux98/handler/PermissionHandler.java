package it.pintux98.handler;

import it.pintux98.server.db.entity.PinGroup;
import it.pintux98.server.db.entity.PinPermission;
import it.pintux98.server.db.entity.StaffUser;
import it.pintux98.server.db.repository.PermissionRepository;
import it.pintux98.server.db.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.Set;

public class PermissionHandler {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private StaffRepository staffRepository;

    private static PermissionHandler instance;

    public static PermissionHandler getInstance() {
        if (instance == null) {
            instance = new PermissionHandler();
        }
        return instance;
    }

    public PermissionHandler() {
    }

    private boolean permissionIsAssignedToAnyGroup(Set<PinGroup> groups, String permission) {
        if (permission.equalsIgnoreCase("pinbot.command.*")) return true;

        PinPermission pinPermission = permissionRepository.findFirstByPermission(permission);
        return groups.stream().anyMatch(pinGroup -> pinGroup.getPermissions().contains(pinPermission));
    }

    public boolean hasPermission(String staffID, String permission) {

        Optional<StaffUser> staffUser = staffRepository.searchByDiscordID(staffID);
        if (!staffUser.isPresent()) {
            return false;
        }

        StaffUser staff = staffUser.get();
        return permissionIsAssignedToAnyGroup(staff.getGroups(), permission);
    }

}
