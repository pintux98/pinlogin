package it.pintux98.common.security;

public class PSecurityException extends Exception {

    public PSecurityException() {
    }

    public PSecurityException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
