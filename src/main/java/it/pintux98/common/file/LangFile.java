package it.pintux98.common.file;

import java.util.Collection;

public class LangFile {

    private ConfigurationAdapter config = new YamlConfig("lang.yml");

    public LangFile() {
        load();
    }

    private void load() {
        config.load();
    }

    public String getString(String path) {
        return config.getString(path, null);
    }

    public int getInt(String path) {
        return config.getInt(path, 0);
    }

    public Collection<?> getList(String path) {
        return config.getList(path, null);
    }

    public long getLong(String path) {
        return config.getLong(path, 0L);
    }
}
