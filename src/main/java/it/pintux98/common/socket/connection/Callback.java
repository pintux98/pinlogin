package it.pintux98.common.socket.connection;

import java.io.IOException;

public interface Callback<A, B> {

    /**
     * Executes a method with two parameters.
     *
     * @param a first parameter
     * @param b second parameter
     */
    void respond(A a, B b) throws IOException;

}
