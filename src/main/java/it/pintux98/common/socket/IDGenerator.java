package it.pintux98.common.socket;

import java.net.Socket;
import java.security.SecureRandom;

public class IDGenerator {

    private final SecureRandom random;

    private static IDGenerator generator;
    public static IDGenerator getGenerator(){
        if(generator == null){
            generator = new IDGenerator();
        }
        return generator;
    }

    public IDGenerator() {
        random = new SecureRandom();
    }

    public synchronized String generateToken(Socket socket) {
        long longToken = Math.abs(random.nextLong() + socket.hashCode());
        return Long.toString(longToken, 16);
    }

}
