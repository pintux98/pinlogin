package it.pintux98.server.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class StaffUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String discordID;

    @OneToMany
    private Set<PinGroup> groups;

    protected StaffUser(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiscordID() {
        return discordID;
    }

    public void setDiscordID(String discordID) {
        this.discordID = discordID;
    }

    public Set<PinGroup> getGroups() {
        return groups;
    }

    public void setGroups(Set<PinGroup> groups) {
        this.groups = groups;
    }
}
