package it.pintux98.common.security;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.AsnResponse;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Location;
import it.pintux98.Core;
import it.pintux98.server.db.entity.LinkedUser;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;

public class IPUtils {

    private final CheckMode mode;
    private DatabaseReader asnReader, cityReader;

    public IPUtils(CheckMode mode) {
        this.mode = mode;
        switch (mode) {
            case STANDARD:
                break;
            case IMPROVED:
                URL asnURL = this.getClass().getClassLoader().getResource("GeoLite2-ASN.mmdb");
                if (asnURL != null) {
                    File asnFile = new File(asnURL.getFile());
                    try {
                        this.asnReader = new DatabaseReader.Builder(asnFile).build();
                    } catch (IOException e) {
                        Core.sendLogMessage(e.getMessage(), true);
                        e.printStackTrace();
                    }
                }

                URL cityURL = this.getClass().getClassLoader().getResource("GeoLite2-City.mmdb");
                if (cityURL != null) {
                    File cityFile = new File(cityURL.getFile());
                    try {
                        this.cityReader = new DatabaseReader.Builder(cityFile).build();
                    } catch (IOException e) {
                        Core.sendLogMessage(e.getMessage(), true);
                        e.printStackTrace();
                    }
                }
            default:
                break;
        }
    }

    public boolean check(LinkedUser savedUser, String loggedIP) {
        switch (mode) {
            case STANDARD:
                return standardCheck(savedUser, loggedIP);
            case IMPROVED:
                try {
                    return improvedCheck(savedUser, loggedIP);
                } catch (Exception e) {
                    e.printStackTrace();
                    Core.sendLogMessage(e.getMessage(), true);
                    return false;
                }
            default:
                return false;
        }
    }

    private boolean standardCheck(LinkedUser user, String loggedIP) {
        return user.getLoginSessions().stream().anyMatch(loginSession -> loginSession.getLoginSessionAddress().getHostName().equalsIgnoreCase(loggedIP));
    }

    private boolean improvedCheck(LinkedUser user, String loggedIP) throws Exception {
        InetAddress userAddress = user.getLoginSessions().stream().filter(loginSession -> loginSession.getLoginSessionAddress().getHostName().equalsIgnoreCase(loggedIP)).findFirst().get().getLoginSessionAddress().getAddress();
        InetAddress loggedAddress = InetAddress.getByName(loggedIP);

        AsnResponse userASN = asnReader.asn(userAddress);
        AsnResponse loggedASN = asnReader.asn(loggedAddress);

        boolean asn = userASN.getAutonomousSystemOrganization() != null && loggedASN.getAutonomousSystemOrganization() != null
                && userASN.getAutonomousSystemOrganization().equals(loggedASN.getAutonomousSystemOrganization());

        CityResponse userResponse = cityReader.city(userAddress);
        CityResponse loggedResponse = cityReader.city(loggedAddress);

        City userCity = userResponse.getCity();
        City loggedCity = loggedResponse.getCity();

        Location userLocation = userResponse.getLocation();
        Location loggedLocation = loggedResponse.getLocation();

        boolean city = userCity != null && loggedCity != null && userLocation != null && loggedLocation != null
                && userCity.getName() != null && loggedCity.getName() != null && userLocation.getLongitude() != null
                && loggedLocation.getLongitude() != null && userLocation.getLatitude() != null && loggedLocation.getLatitude() != null
                && userCity.getName().equals(loggedCity.getName()) && userLocation.getLatitude().equals(loggedLocation.getLatitude())
                && userLocation.getLongitude().equals(loggedLocation.getLongitude());


        return asn && city;
    }

}
