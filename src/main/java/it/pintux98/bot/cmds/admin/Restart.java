package it.pintux98.bot.cmds.admin;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.PCommand;
import it.pintux98.bot.util.Utils;
import net.dv8tion.jda.api.Permission;

public class Restart extends PCommand {

    public Restart() {
        this.name = "restart";
        this.help = "restart";
        this.guildOnly = false;
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        restart(true);
    }

    private void restart(boolean connection) {
        if (connection) {
            Core.getInstance().getServer().stopServer();
            Core.getInstance().loadServerSocket();
        }
        Utils.getInstance().loadChannels();
    }

    @Override
    public String getPermission() {
        return "restart";
    }
}
