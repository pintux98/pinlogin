package it.pintux98.server.db.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LinkedUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String discordID;
    private String uuid;
    private String name;
    private String backupCode;
    private Timestamp created;
    private boolean linked;
    @OneToMany
    private Set<LoginSession> loginSessions = new HashSet<>();

    protected LinkedUser() {
    }

    public LinkedUser(String name, String uuid, String backupCode) {
        this.name = name;
        this.uuid = uuid;
        this.backupCode = backupCode;
        this.created = Timestamp.from(Instant.now());
        this.linked = false;
    }

    public Long getId() {
        return id;
    }

    public String getDiscordID() {
        return discordID;
    }

    public void setDiscordID(String discordID) {
        this.discordID = discordID;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getBackupCode() {
        return backupCode;
    }

    public void setBackupCode(String backupCode) {
        this.backupCode = backupCode;
    }

    public Timestamp getCreated() {
        return created;
    }

    public boolean isLinked() {
        return linked;
    }

    public void setLinked(boolean linked) {
        this.linked = linked;
    }


    public Set<LoginSession> getLoginSessions() {
        return loginSessions;
    }
}
