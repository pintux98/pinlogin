package it.pintux98.server.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
public class LoginSession implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private Timestamp loginTime;
    private String host;
    private int port;

    protected LoginSession() {
    }

    public LoginSession(String host, int port) {
        this.host = host;
        this.port = port;
        this.loginTime = Timestamp.from(Instant.now());
    }

    public Long getId() {
        return id;
    }

    public Timestamp getLoginTime() {
        return loginTime;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public InetSocketAddress getLoginSessionAddress() {
        return new InetSocketAddress(host, port);
    }
}
