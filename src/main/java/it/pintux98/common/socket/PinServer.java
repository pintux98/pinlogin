package it.pintux98.common.socket;

import it.pintux98.common.socket.connection.Callback;
import it.pintux98.common.socket.connection.packet.Packet;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.Set;

public interface PinServer {

    void runServer();
    void stopServer();

    void addClient(PinClient client);
    PinClient castFromSocket(Socket socket);
    void removeClient(PinClient con);

    void registerPacket(String key, Callback<Packet, Socket> task);
    void sendPacket(PinClient receiver, Packet packet);
    void broadcastPacket(Packet packet);

    boolean isRunning();
    Map<String, Callback<Packet, Socket>> getRegistry();
    Set<PinClient> getClients();

    void debug(String debug);

}
