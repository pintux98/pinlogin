package it.pintux98.bot.config;

import it.pintux98.Core;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class FileIO {

    private final String folder, fileName;

    public FileIO(String folder, String fileName) {
        this.folder = folder;
        this.fileName = fileName;
    }

    public File getFile(boolean returnDefault) {
        File fl = new File("PinLoginPremium/" + folder + "/" + fileName);
        if (!fl.exists()) {
            fl.getParentFile().mkdirs();
            if (returnDefault)
                copy(getResource(fileName), fl);
            return fl;
        }
        return fl;
    }

    private InputStream getResource(String filename) {
        if (filename == null) {
            throw new IllegalArgumentException("Filename cannot be null");
        }
        try {
            URL url = Core.class.getClassLoader().getResource(filename);
            if (url == null) {
                return null;
            }
            URLConnection connection = url.openConnection();
            connection.setUseCaches(false);
            return connection.getInputStream();
        } catch (IOException ex) {
            return null;
        }
    }

    private void copy(final InputStream in, final File file) {
        try {
            final OutputStream out = new FileOutputStream(file);
            final byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] getFiles() {
        File file = new File("PinLoginPremium/" + folder);
        return file.list();
    }

}
