package it.pintux98.bot.cmds.admin;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.PCommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;

import java.awt.*;
import java.time.Instant;

public class Clients extends PCommand {

    public Clients() {
        this.name = "clients";
        this.help = "clients - Shows all pinlogin connected lobbies";
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = false;
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        EmbedBuilder builder = new EmbedBuilder()
                .setColor(Color.GREEN)
                .setTitle("Connected clients")
                .setFooter("Author: pintux#2007")
                .setTimestamp(Instant.now());
        if (Core.getInstance().getServer().getClients().isEmpty()) {
            builder.setDescription("NO CONNECTED LOBBIES FOUND!");
        } else {
            Core.getInstance().getServer().getClients()
                    .forEach(con ->
                            builder.addField(con.getClientID(), "ServerID: " + con.getServerID() + "\nClientIP: " + con.getSocket().getRemoteSocketAddress(), true)
                    );
        }

        event.replyEmbeds(builder.build()).queue();
    }

    @Override
    public String getPermission() {
        return "clients";
    }

    @Override
    public boolean isServerDependent() {
        return true;
    }
}
