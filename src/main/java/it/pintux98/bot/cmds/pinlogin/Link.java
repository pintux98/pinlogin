package it.pintux98.bot.cmds.pinlogin;

import com.jagrosh.jdautilities.command.SlashCommandEvent;
import it.pintux98.Core;
import it.pintux98.bot.cmds.CommandOption;
import it.pintux98.bot.cmds.PCommand;
import it.pintux98.bot.config.FileManager;
import it.pintux98.bot.config.data.GuildData;
import it.pintux98.bot.config.data.LangData;
import it.pintux98.bot.util.Utils;
import it.pintux98.server.db.entity.LinkedUser;
import it.pintux98.server.db.repository.LinkRepository;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

public class Link extends PCommand {

    @Autowired
    private LinkRepository linkRepository;

    public Link() {
        this.name = "link";
        this.help = "Link player using the secret code";
        this.guildOnly = true;
        this.options = Collections.singletonList(
                new OptionData(OptionType.STRING, CommandOption.CODE.getOption(), "Secret code that you got from mc lobby"));
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        FileManager fileManager = Core.getFileManager();
        GuildData guildData = fileManager.getGuildData();
        LangData langData = fileManager.getLangData();
        if (event.getChannel().getIdLong() != guildData.getPinloginchannel()) {
            TextChannel channel = event.getGuild().getTextChannelById(guildData.getPinloginchannel());

            String text = channel != null ? channel.getAsMention() : "#NOT_SET";

            event.reply(langData.getCommandwrongchannel().replaceAll("%channel%", text)).queue();
            return;
        }

        List<LinkedUser> linkedUsers = Utils.getInstance().getLinkedUsers(event);
        if (linkedUsers.isEmpty()) {
            event.reply("User not found").setEphemeral(true).queue();
            return;
        }

        OptionMapping codeOption = event.getOption(CommandOption.CODE.getOption());
        if (codeOption != null) {
            String code = codeOption.getAsString();
            LinkedUser user = linkedUsers.get(0);
            user.setDiscordID(event.getUser().getId());
            user.setLinked(true);
            user.setBackupCode(String.valueOf(code.hashCode())); //TODO: generate better code
            linkRepository.save(user);
            event.reply("LINK SUCCESS - SAVE THIS CODE: " + code.hashCode()).setEphemeral(true).queue();
        }
    }

    @Override
    public String getPermission() {
        return "link";
    }

    @Override
    public boolean isServerDependent() {
        return true;
    }
}
